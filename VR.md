# Gaia Sky Virtual Reality

Gaia Sky VR is **beta** software!

Gaia Sky VR uses the [OpenXR API](https://registry.khronos.org/OpenXR/) to bring an immersive experience regardless of the VR system used.

The most up-to-date information on Gaia Sky VR, as well as how to install and run it, is available in the official Gaia Sky VR documentation:

- [Gaia Sky VR documentation](https://gaia.ari.uni-heidelberg.de/gaiasky/docs/Gaia-sky-vr.html)

##  More info

- [Gaia Sky documentation](https://gaia.ari.uni-heidelberg.de/gaiasky/docs)
- [The project's main README file](README.md)
- [Gaia Sky website](https://zah.uni-heidelberg.de/gaia/outreach/gaiasky/)
