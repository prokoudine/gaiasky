import org.gradle.nativeplatform.platform.internal.DefaultNativePlatform

plugins {
  id "com.install4j.gradle" version "10.0.4"
  id "com.dorongold.task-tree" version "1.5"
  id "de.undercouch.download" version "4.1.1"
}


allprojects {
    apply plugin: "eclipse"
    apply plugin: "idea"

    ext {
        appName = "GaiaSky"
        gdxVersion = "1.12.0"
        gdxcontrollersVersion = "2.2.2"
        lwjglVersion = "3.3.2"
        jcommanderVersion = "1.82"
        slf4jVersion = "2.0.+"
        sparkjavaVersion = "2.9.+"
        jafamaVersion = "2.3.+"
        compressVersion = "1.23.0"
        commonsioVersion = "2.+"
        py4jVersion = "0.10.9.+"
        oshiVersion = "6.4.+"
        stilVersion = "4.1.+"
        jsampVersion = "1.3.+"
        apfloatVersion = "1.10.+"
        jacksonVersion = "2.15.+"
        joiseVersion = "1.1.0"
        ashleyVersion = "1.7.4"
        gltfVersion = "-SNAPSHOT"
        jgltfVersion = "2.0.+"
    }

    repositories {
        mavenCentral()
        maven { url 'https://jitpack.io' }
    }
}

project(":core") {
    apply plugin: "java-library"

    def cmd = "git describe --abbrev=0 --tags HEAD"
    def proc = cmd.execute()
    project.ext.tag = proc.text.trim()

    cmd = "git rev-parse --short HEAD"
    proc = cmd.execute()
    project.ext.rev = proc.text.trim()

    if (DefaultNativePlatform.currentOperatingSystem.windows) {
        project.ext.system = DefaultNativePlatform.currentOperatingSystem.getName() + " " + DefaultNativePlatform.currentArchitecture.getName()
    } else {
        cmd = "uname -snmr"
        proc = cmd.execute()
        project.ext.system = proc.text.trim()
    }

    project.ext.builder = System.properties["user.name"]
    project.ext.buildtime = new Date()
    version = "$tag"

    println ""
    println "CURRENT SYSTEM"
    println "=============="
    println "java version: " + JavaVersion.current().toString()
    println "system: $system"

    println ""
    println "GAIA SKY"
    println "========"
    println "git tag: $tag"
    println "git rev: $rev"
    println "buildtime: $buildtime"
    println "builder: $builder"
    println ""

    // Set some build variables
    project.ext.baseDir = System.getProperty("user.dir")
    project.ext.tagRev = project.tag + "." + project.rev
    project.ext.distName = "gaiasky-$tagRev"
    project.ext.releasesDir = "$baseDir/releases"
    project.ext.distDir = "$releasesDir/$distName"
    project.ext.packageName = "packages-$tagRev"
    project.ext.packageDir = "$releasesDir/$packageName"

    println ""
    println "BUILD VARIABLES AND INFO"
    println "========================"
    println "base dir: $baseDir"
    println "tag.rev: $tagRev"
    println "dist name: $distName"
    println "dist dir: $distDir"
    println "packages name: $packageName"
    println "packages dir: $packageDir"
    println ""

    tasks.withType(JavaCompile).configureEach { options.compilerArgs << "-parameters" }

    dependencies {
        // *****************************
        // GENERATE SKINS (PackUITextures)
        // *****************************
        //api "com.badlogicgames.gdx:gdx-tools:$gdxVersion"

        // ************
        // REGULAR DEPS
        // ************

        // OpenXR
        api group: "org.lwjgl", name: "lwjgl-openxr", version: "$lwjglVersion"
        api "org.lwjgl:lwjgl-openxr:$lwjglVersion:natives-linux"
        api "org.lwjgl:lwjgl-openxr:$lwjglVersion:natives-windows"

        // LibGDX
        api group: "com.badlogicgames.gdx", name: "gdx", version: "$gdxVersion"
        api group: "com.badlogicgames.gdx", name: "gdx-backend-lwjgl3", version: "$gdxVersion"
        api "com.badlogicgames.gdx:gdx-platform:$gdxVersion:natives-desktop"

        // gdx-controllers:2.+ bases up on SDL 2.0.12
        api group: "com.badlogicgames.gdx-controllers", name: "gdx-controllers-core", version: "$gdxcontrollersVersion"
        api group: "com.badlogicgames.gdx-controllers", name: "gdx-controllers-desktop", version: "$gdxcontrollersVersion"

        // Ashley
        api group: "com.badlogicgames.ashley", name: "ashley", version: "$ashleyVersion"

        // STIL library to load datasets
        api group: "uk.ac.starlink", name: "stil", version: "$stilVersion"
        // JSAMP for SAMP communication
        api group: "uk.ac.starlink", name: "jsamp", version: "$jsampVersion"
        // Apfloat for arbitrary precision floating point numbers
        api group: "org.apfloat", name: "apfloat", version: "$apfloatVersion"

        // File utilities
        api group: "commons-io", name: "commons-io", version: "$commonsioVersion"
        // Compression
        api group: "org.apache.commons", name: "commons-compress", version: "$compressVersion"
        // Command line arguments
        api group: "com.beust", name: "jcommander", version: "$jcommanderVersion"
        // Fast math
        api group: "net.jafama", name: "jafama", version: "$jafamaVersion"

        // Scripting
        api group: "net.sf.py4j", name: "py4j", version: "$py4jVersion"
        // System information
        api group: "com.github.oshi", name: "oshi-core", version: "$oshiVersion"
        // Yaml
        api group: "com.fasterxml.jackson.core", name: "jackson-databind", version: "$jacksonVersion"
        api group: "com.fasterxml.jackson.dataformat", name: "jackson-dataformat-yaml", version: "$jacksonVersion"
        api group: "com.fasterxml.jackson.datatype", name: "jackson-datatype-jsr310", version: "$jacksonVersion"

        // Joise
        api group: "com.sudoplay.joise", name: "joise", version: "$joiseVersion"

        // ****************
        // REST SERVER DEPS
        // ****************
        api group: "org.slf4j", name: "slf4j-nop", version: "$slf4jVersion"
        api group: "com.sparkjava", name: "spark-core", version: "$sparkjavaVersion"

        // *************
        // INTERNAL DEPS
        // *************
        api files("../assets")
    }

    jar {
        manifest {
            attributes "Implementation-Title": "Gaia Sky",
            "Implementation-Version": archiveVersion
        }
        from("../assets") {
            include "font/main-font.fnt"
            include "font/main-font.png"
            include "font/font2d.fnt"
            include "font/font2d.png"
            include "font/font-titles.fnt"
            include "font/font-titles.png"
            include "icon/gs_icon.png"
            include "icon/gsvr_icon.png"
            include "icon/gsascii.txt"
            include "img/**"
            include "text/**"
            include "shader/**"
            include "archetypes/**"
            include "data/**"
            include "shaders/**"
            include "skins/**"
            exclude "skins/raw"
            exclude "assets-bak/**"
        }
        from("build/classes/main") { include "**" }
    }

}
